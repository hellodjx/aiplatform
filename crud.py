from collections import namedtuple
from typing import Union
from sqlalchemy.orm import Session
from models import OCRType
import models
import schemas
from passlib.hash import pbkdf2_sha256
import time
import json
import utils

PublicHistoryEntry = namedtuple("PublicHistoryEntry", ['ocr_type', 'result', 'time', 'thumb', 'hash'])


async def get_user(db: Session, user_id: int):
    return db.query(models.User).filter(models.User.id == user_id).first()


async def get_user_by_name(db: Session, name: str):
    return db.query(models.User).filter(models.User.user_name == name).first()


async def add_user(db: Session, user: schemas.UserCreate):
    hashed_password = pbkdf2_sha256.hash(user.hashed_password)
    db_user = models.User(user_name=user.user_name, hashed_password=hashed_password, photo_path=user.photo_path)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


async def set_password(db: Session, name: str, new_password: str):
    user = await get_user_by_name(db, name)
    user.hashed_password = pbkdf2_sha256.hash(new_password)
    db.commit()


async def is_user_exist(db: Session, name: str) -> bool:
    if await get_user_by_name(db, name):
        return True
    else:
        return False


async def check_password(db: Session, name: str, password: str) -> bool:
    user = await get_user_by_name(db, name)
    hash_password = user.hashed_password

    if pbkdf2_sha256.verify(password, hash_password):
        return True
    else:
        return False


async def add_file_hash_to_user_history(db: Session, name: str, file_hash: str, ocr_type: str):
    user = await get_user_by_name(db, name)
    ocr_result = db.query(models.OCRResult).filter(models.OCRResult.file_hash == file_hash).filter(
        models.OCRResult.ocr_type == ocr_type).first()
    user.ocr_results.append(ocr_result)
    db.commit()


async def get_user_history(db: Session, name: str, ocr_type: str) -> list:
    user = await get_user_by_name(db, name)
    user_id = user.id
    if user_id:
        ocr_results = db.query(models.OCRResult).join(models.OCRResult.user_ids).filter(
            models.OCRResult.ocr_type == ocr_type).filter(models.User.id == user_id).all()
        return list(ocr_results)

    return []


async def del_file_hash_to_user_history(db: Session, name: str, file_hash: str, ocr_type: str):
    user = await get_user_by_name(db, name)
    ocr_result = db.query(models.OCRResult).filter(models.OCRResult.file_hash == file_hash).filter(
        models.OCRResult.ocr_type == ocr_type).first()
    user.ocr_results.remove(ocr_result)
    db.commit()


async def add_result(db: Session, result: str, file_hash: str, ocr_type: str):
    db_ocr_result = models.OCRResult(file_hash=file_hash, result=result, ocr_type=ocr_type,
                                     time_add2db=str(time.time()))
    db.add(db_ocr_result)
    db.commit()
    db.refresh(db_ocr_result)
    return db_ocr_result


async def get_result(db: Session, file_hash: str, ocr_type: str) -> str:
    ocr_result = db.query(models.OCRResult).filter(models.OCRResult.file_hash == file_hash).filter(
        models.OCRResult.ocr_type == ocr_type).first()
    if ocr_result:
        return ocr_result.result
    return ""


async def set_user_photo_path(db: Session, name: str, photo_path: str):
    user = await get_user_by_name(db, name)
    if user:
        user.photo_path = photo_path
        db.commit()


async def get_user_photo_path(db: Session, name: str) -> str:
    user = await get_user_by_name(db, name)
    if user:
        return user.photo_path
    else:
        raise RuntimeError


async def get_ocr_result_and_add_to_db(db: Session, img_path: str, ocr_type: OCRType) -> Union[list, str]:
    file_hash = await utils.get_file_hash(img_path)
    ocr_result = await get_result(db, file_hash, ocr_type)
    if ocr_result:
        return json.loads(ocr_result)
    ocr_result = await utils.ocr(img_path, ocr_type)
    ocr_result = [i for i in ocr_result if i]
    await add_result(db, json.dumps(ocr_result), file_hash, ocr_type)

    return ocr_result


async def get_all_history_entry(db: Session, name: str) -> list:
    history_entry_list = []
    for ocr_type in OCRType:
        ocr_type = str(ocr_type)
        user_histories = await get_user_history(db, name, ocr_type)
        for ocr_result in user_histories:
            try:
                result = json.loads(ocr_result.result)
                thumb_storage_path = await utils.get_thumb_storage_path(ocr_result.file_hash)
                history_entry = PublicHistoryEntry(ocr_type, result, ocr_result.time_add2db,
                                                   thumb_storage_path,
                                                   ocr_result.file_hash)
                history_entry_list.append(history_entry)
            except:
                pass

    return history_entry_list
