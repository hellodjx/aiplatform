from models import *
from conf import user_photo_path
import os

test_db = RedisDB(is_test=True)


def test_add_user():
    test_db.add_user("test1", "123", os.path.join(user_photo_path, "test.png"))
    test_db.add_user("test2", "456", os.path.join(user_photo_path, "test.png"))


def test_auth():
    assert test_db.check_password("test1", "123") == True
    assert test_db.check_password("test1", "1") == False
    assert test_db.check_password("null", "none") == False


def test_add_file_hash_to_user_history():
    test_db.add_file_hash_to_user_history("test1", "3ce1f708a8d6a076ec90bebd1e61763de77dc64e", "normal")


def test_get_user_history_and_time():
    assert test_db.get_user_history_and_time("test1", "normal")[0][0] == "3ce1f708a8d6a076ec90bebd1e61763de77dc64e"


def test_get_user_history():
    assert test_db.get_user_history("test1", "normal")[0] == "3ce1f708a8d6a076ec90bebd1e61763de77dc64e"


def test_add_result_hash():
    test_db.add_result_hash("中文测试", "3ce1f708a8d6a076ec90bebd1e61763de77dc64e", "normal")


def test_get_result():
    assert test_db.get_result("3ce1f708a8d6a076ec90bebd1e61763de77dc64e", "normal") == "中文测试"


if __name__ == "__main__":
    test_add_user()
    test_auth()
    test_add_file_hash_to_user_history()
    test_get_user_history_and_time()
    test_get_user_history()
    test_add_result_hash()
    test_get_result()
