import hashlib
import os
import re
import shutil
import time
from enum import Enum
from typing import Optional
import cv2
from fastapi import UploadFile
# from chineseocr.model import text_predict
from rapidocr_onnxruntime import RapidOCR
from conf import tmp_file_path, history_thumb, user_photo_path
from crud import OCRType


OCREngine = RapidOCR()


class CardType(str, Enum):
    id_card = "ID Card"
    union_pay_card = "Union Pay Card"

    def __str__(self):
        return self.value


def get_ocr_result(img_path: str):
    result, elapse = OCREngine(img_path)
    text = [i[1] for i in result]
    return text


async def get_file_hash(file_path: str) -> str:
    hasher = hashlib.sha1()
    with open(file_path, 'rb') as f:
        buf = f.read()
        hasher.update(buf)

    return hasher.hexdigest()


async def get_thumb_storage_path(thumb_hash: str):
    return os.path.join(history_thumb, thumb_hash + ".jpg")


async def save_upload_img(file: UploadFile) -> str:
    if not os.path.exists(tmp_file_path):
        os.makedirs(tmp_file_path)
    storage_path = os.path.join(tmp_file_path, str(time.time_ns()) + file.filename)
    with open(storage_path, "wb") as buffer:
        shutil.copyfileobj(file.file, buffer)

    # generate thumb
    thumb_hash = await get_file_hash(storage_path)
    img = cv2.imread(storage_path)
    resize_img = cv2.resize(img, (120, 100))
    thumb_storage_path = await get_thumb_storage_path(thumb_hash)
    cv2.imwrite(thumb_storage_path, resize_img)

    return storage_path


async def save_user_photo(file: UploadFile) -> str:
    storage_path = os.path.join(user_photo_path, str(time.time_ns()) + file.filename)
    with open(storage_path, "wb") as buffer:
        shutil.copyfileobj(file.file, buffer)

    return storage_path


async def guess_card_type(ocr_result: list) -> Optional[CardType]:
    card_num = max(ocr_result, key=len)

    id_card_pattern = "^" + "\\d{6}" + "(18|19|([23]\\d))\\d{2}" + "((0[1-9])|(10|11|12))" + \
                      "(([0-2][1-9])|10|20|30|31)" + "\\d{3}" + "[0-9Xx]"
    prog = re.compile(id_card_pattern)
    if prog.search(card_num):
        return CardType.id_card
    
    if 15 < len(card_num) <= 19 and card_num[0:2] == "62":
        return CardType.union_pay_card

    return None


async def ocr_normal(img_path: str) -> list:
    result = get_ocr_result(img_path)

    return result


async def ocr_book(img_path: str) -> list:
    result = get_ocr_result(img_path)

    return result


async def ocr_card(img_path: str) -> list:
    result = get_ocr_result(img_path)
    card_type = await guess_card_type(result)
    if card_type:
        result = [max(result, key=len)]
        result.insert(0, card_type)
        return result
    else:
        raise RuntimeError


async def ocr(img_path: str, ocr_type: OCRType) -> list:
    if ocr_type == OCRType.normal:
        return await ocr_normal(img_path)
    elif ocr_type == OCRType.book:
        return await ocr_book(img_path)
    elif ocr_type == OCRType.card:
        return await ocr_card(img_path)
    else:
        raise TypeError
