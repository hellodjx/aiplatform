import enum
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, ForeignKey, Integer, String, Enum, Table
from sqlalchemy.orm import relationship
from conf import *

SQLALCHEMY_DATABASE_URL = f"postgresql://{postgre_user}:{postgre_password}@{postgre_address}/{postgre_dbname}"
engine = create_engine(SQLALCHEMY_DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base()


class OCRType(str, enum.Enum):
    normal = "normal"
    book = "book"
    card = "card"

    def __str__(self):
        return self.value


UserOCRResult = Table("UserOCRResult",
                      Base.metadata,
                      Column('id', Integer, primary_key=True),
                      Column('user_id', Integer, ForeignKey('User.id')),
                      Column('ocr_result_id', Integer, ForeignKey('OCRResult.id')))


class User(Base):
    __tablename__ = "User"

    id = Column(Integer, primary_key=True, index=True)
    user_name = Column(String, unique=True)
    hashed_password = Column(String)
    photo_path = Column(String)

    ocr_results = relationship("OCRResult", secondary=UserOCRResult, backref='User')


class OCRResult(Base):
    __tablename__ = "OCRResult"

    id = Column(Integer, primary_key=True)
    file_hash = Column(String)
    result = Column(String)
    time_add2db = Column(String)
    ocr_type = Column(Enum(OCRType))

    user_ids = relationship("User", secondary=UserOCRResult, backref='OCRResult')
