import redis
import time
import os
from passlib.hash import pbkdf2_sha256
from conf import *


class RedisDB:
    def __init__(self, is_test: bool = False):
        if is_test:
            if not test_redis_password:
                self.redis_server = redis.StrictRedis(host=test_redis_host, port=test_redis_port,
                                                      db=redis_db, charset="utf-8",
                                                      decode_responses=True)
            else:
                self.redis_server = redis.StrictRedis(host=test_redis_host, port=test_redis_port,
                                                      password=test_redis_password, db=test_redis_db,
                                                      charset="utf-8",
                                                      decode_responses=True)
        else:
            if not redis_password:
                self.redis_server = redis.StrictRedis(host=redis_host, port=redis_port,
                                                      db=redis_db, charset="utf-8",
                                                      decode_responses=True)
            else:
                self.redis_server = redis.StrictRedis(host=redis_host, port=redis_port,
                                                      password=redis_password, db=redis_db,
                                                      charset="utf-8",
                                                      decode_responses=True)

        if not self.redis_server.get('user_id'):
            self.redis_server.set('user_id', 1)

    def add_user(self, name: str, password: str, photo_path: str = os.path.join(user_photo_path, "default.png")):
        user_id = self.redis_server.get('user_id')
        hash_password = pbkdf2_sha256.hash(password)
        self.redis_server.hmset(f"userdata_{user_id}",
                                {"name": name, "password": hash_password, "photo_path": photo_path})
        self.redis_server.hset("user_name_id", name, user_id)
        self.redis_server.incr('user_id')

    def get_password(self, name: str) -> str:
        user_id = self.redis_server.hget("user_name_id", name)
        if user_id:
            password = self.redis_server.hget(f"userdata_{user_id}", "password")
            return password
        else:
            raise RuntimeError

    def set_password(self, name: str, new_password: str):
        user_id = self.redis_server.hget("user_name_id", name)
        if user_id:
            hash_password = pbkdf2_sha256.hash(new_password)
            photo_path = self.redis_server.hget(f"userdata_{user_id}", "photo_path")
            self.redis_server.hmset(f"userdata_{user_id}",
                                    {"name": name, "password": hash_password, "photo_path": photo_path})
        else:
            raise RuntimeError

    def is_user_exist(self, name: str) -> bool:
        try:
            self.get_password(name)
            return True
        except Exception:
            return False

    def check_password(self, name: str, password: str) -> bool:
        try:
            hash_password = self.get_password(name)
        except Exception:
            return False
        if pbkdf2_sha256.verify(password, hash_password):
            return True
        else:
            return False

    def add_file_hash_to_user_history(self, name: str, file_hash: str, ocr_type: str):
        user_id = self.redis_server.hget("user_name_id", name)
        if user_id:
            self.redis_server.zadd(f"{ocr_type}_history_{user_id}", {file_hash: time.time()})

    def get_user_history_and_time(self, name: str, ocr_type: str) -> list:
        user_id = self.redis_server.hget("user_name_id", name)
        if user_id:
            hash_time_pairs = self.redis_server.zrangebyscore(f"{ocr_type}_history_{user_id}", 0, time.time(),
                                                              withscores=True)
            return hash_time_pairs

        return []

    def get_user_history(self, name: str, ocr_type: str) -> list:
        user_id = self.redis_server.hget("user_name_id", name)
        if user_id:
            history_hash_list = self.redis_server.zrangebyscore(f"{ocr_type}_history_{user_id}", 0, time.time())
            return history_hash_list

        return []

    def del_file_hash_to_user_history(self, name: str, file_hash: str, ocr_type: str):
        user_id = self.redis_server.hget("user_name_id", name)
        if user_id:
            self.redis_server.zrem(f"{ocr_type}_history_{user_id}", file_hash)

    def add_result_hash(self, result: str, file_hash: str, ocr_type: str):
        self.redis_server.hset(ocr_type, file_hash, result)

    def get_result(self, file_hash: str, ocr_type: str) -> str:
        result = self.redis_server.hget(ocr_type, file_hash)
        if result:
            return result
        return ""

    def set_user_photo_path(self, name: str, photo_path: str):
        user_id = self.redis_server.hget("user_name_id", name)
        if user_id:
            password = self.redis_server.hget(f"userdata_{user_id}", "password")
            self.redis_server.hmset(f"userdata_{user_id}",
                                    {"name": name, "password": password, "photo_path": photo_path})

    def get_user_photo_path(self, name: str) -> str:
        user_id = self.redis_server.hget("user_name_id", name)
        if user_id:
            photo_path = self.redis_server.hget(f"userdata_{user_id}", "photo_path")
            return photo_path
        else:
            raise RuntimeError