import json
from fastapi import FastAPI, File, UploadFile, Form, Depends, Request, HTTPException
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles
from fastapi.responses import JSONResponse
from fastapi_jwt_auth import AuthJWT
from fastapi_jwt_auth.exceptions import AuthJWTException
from datetime import timedelta
from pydantic import BaseModel
from conf import tmp_file_path, SECRET_KEY, user_photo_path, history_thumb
from models import *
from utils import *
from crud import *

app = FastAPI()
app.mount("/assets", StaticFiles(directory="assets"), name="assets")
app.mount("/user-static", StaticFiles(directory=user_photo_path), name="user-static")
app.mount("/thumbs", StaticFiles(directory=history_thumb), name="thumbs")
templates = Jinja2Templates(directory="templates/")
models.Base.metadata.create_all(bind=engine)


class Settings(BaseModel):
    authjwt_secret_key: str = SECRET_KEY
    authjwt_token_location: set = {"cookies"}
    authjwt_cookie_csrf_protect: bool = True
    # Only allow JWT cookies to be sent over https
    authjwt_cookie_secure: bool = False


@AuthJWT.load_config
def get_config():
    return Settings()


@app.exception_handler(AuthJWTException)
def auth_jwt_exception_handler(request: Request, exc: AuthJWTException):
    return JSONResponse(
        status_code=exc.status_code,
        content={"detail": exc.message}
    )


# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


class InfoForm(BaseModel):
    password: str = None


@app.post("/token")
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db),
                                 authorize: AuthJWT = Depends()):
    if not await check_password(db, form_data.username, form_data.password):
        raise HTTPException(status_code=401, detail="Bad username or password")

    access_token = authorize.create_access_token(subject=form_data.username)
    authorize.set_access_cookies(access_token)

    return {"access_token": access_token, "token_type": "bearer"}


@app.delete('/logout')
async def logout(authorize: AuthJWT = Depends()):
    authorize.jwt_required()

    authorize.unset_jwt_cookies()
    return {"msg": "Successfully logout"}


@app.get("/")
async def index_page(request: Request):
    return templates.TemplateResponse('home.html', context={'request': request})


@app.get("/register/")
async def register_page(request: Request):
    return templates.TemplateResponse('register.html', context={'request': request})


@app.get("/login/")
async def login_page(request: Request):
    return templates.TemplateResponse('login.html', context={'request': request})


@app.get("/normal-ocr/")
async def normal_ocr_page(request: Request):
    return templates.TemplateResponse('ocr.html', context={'request': request, 'ocr_type': "normal"})


@app.get("/book-ocr/")
async def book_ocr_page(request: Request):
    return templates.TemplateResponse('ocr.html', context={'request': request, 'ocr_type': "book"})


@app.get("/card-ocr/")
async def card_ocr_page(request: Request):
    return templates.TemplateResponse('ocr.html', context={'request': request, 'ocr_type': "card"})


@app.get("/my/")
async def user_page(request: Request, authorize: AuthJWT = Depends(), db: Session = Depends(get_db)):
    authorize.jwt_required()

    username = authorize.get_jwt_subject()
    photo_path = await get_user_photo_path(db, username)
    return templates.TemplateResponse('userinfo.html', context={'request': request, 'username': username,
                                                                'user_photo_path': photo_path})


@app.get("/my/info")
async def user_page_info(request: Request, authorize: AuthJWT = Depends(), db: Session = Depends(get_db)):
    authorize.jwt_required()

    username = authorize.get_jwt_subject()
    photo_path = await get_user_photo_path(db, username)
    return templates.TemplateResponse('userinfo.html', context={'request': request, 'username': username,
                                                                'user_photo_path': photo_path})


@app.get("/my/history")
async def user_page_history(request: Request, authorize: AuthJWT = Depends(), db: Session = Depends(get_db)):
    authorize.jwt_required()

    username = authorize.get_jwt_subject()
    photo_path = await get_user_photo_path(db, username)
    return templates.TemplateResponse('userhistory.html', context={'request': request, 'username': username,
                                                                   'user_photo_path': photo_path})


@app.get("/user/name")
async def get_username(authorize: AuthJWT = Depends()):
    authorize.jwt_required()

    username = authorize.get_jwt_subject()
    return {"name": username}


@app.get("/user/photo")
async def get_user_photo(authorize: AuthJWT = Depends(), db: Session = Depends(get_db)):
    authorize.jwt_required()

    username = authorize.get_jwt_subject()
    photo_path = await get_user_photo_path(db, username)
    return {"user_photo_path": photo_path}


@app.post("/user/")
async def add_new_user(name: str = Form(...), password: str = Form(...), db: Session = Depends(get_db)):
    if not await is_user_exist(db, name):
        data = {'user_name': name, 'hashed_password': password}
        await add_user(db, schemas.UserCreate(**data))
        return {"result": "ok"}
    else:
        return {"result": "Already register"}


@app.patch("/user/password/")
async def change_user_password(info_form: InfoForm, authorize: AuthJWT = Depends(), db: Session = Depends(get_db)):
    authorize.jwt_required()

    username = authorize.get_jwt_subject()
    if info_form.password:
        await set_password(db, username, info_form.password)


@app.post("/user/photo/")
async def change_user_photo(file: UploadFile = File(...), authorize: AuthJWT = Depends(),
                            db: Session = Depends(get_db)):
    authorize.jwt_required()

    username = authorize.get_jwt_subject()
    storage_path = await save_user_photo(file)
    await set_user_photo_path(db, username, storage_path)


@app.get("/user/history/")
async def get_history_list(authorize: AuthJWT = Depends(), db: Session = Depends(get_db)):
    authorize.jwt_required()

    username = authorize.get_jwt_subject()
    history_entry_list = await get_all_history_entry(db, username)
    return history_entry_list


@app.get("/user/history/{ocr_type}/{file_hash}")
async def get_history(file_hash: str, ocr_type: str, db: Session = Depends(get_db)):
    result = await get_result(db, file_hash, ocr_type)
    try:
        result = json.loads(result)
    except Exception:
        pass
    return {"result": result}


@app.post("/user/history/{ocr_type}/")
async def add_history(ocr_type: OCRType, file: UploadFile = File(...), authorize: AuthJWT = Depends(),
                      db: Session = Depends(get_db)):
    authorize.jwt_required()

    storage_path = await save_upload_img(file)
    username = authorize.get_jwt_subject()
    ocr_result = await get_ocr_result_and_add_to_db(db, storage_path, ocr_type)
    file_hash = await get_file_hash(storage_path)
    await add_file_hash_to_user_history(db, username, file_hash, ocr_type)
    return {"result": ocr_result}


@app.delete("/user/history/{ocr_type}/{file_hash}")
async def del_history(file_hash: str, ocr_type: str, authorize: AuthJWT = Depends(),
                      db: Session = Depends(get_db)):
    authorize.jwt_required()

    username = authorize.get_jwt_subject()
    await del_file_hash_to_user_history(db, username, file_hash, ocr_type)
    return {"result": "ok"}


@app.post("/ocr-result/{ocr_type}/")
async def add_ocr_result(ocr_type: OCRType, file: UploadFile = File(...),
                         db: Session = Depends(get_db)):
    storage_path = await save_upload_img(file)
    ocr_result = await get_ocr_result_and_add_to_db(db, storage_path, ocr_type)

    return {"result": ocr_result}
