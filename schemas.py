from typing import List
from pydantic import BaseModel
import os
from models import OCRType
from conf import user_photo_path


class OCRResultBase(BaseModel):
    file_hash: str
    result: str
    time_add2db: str
    ocr_type: OCRType


class OCRResultCreate(OCRResultBase):
    pass


class OCRResult(OCRResultBase):
    id: int
    users: List["User"] = []

    class Config:
        orm_mode = True
        use_enum_values = True


class UserBase(BaseModel):
    user_name: str


class UserCreate(UserBase):
    hashed_password: str
    photo_path: str = os.path.join(user_photo_path, "default.png")


class User(UserBase):
    id: int
    ocr_results: List[OCRResult] = []

    class Config:
        orm_mode = True